# README #

This document details what is needed to get this project up and running

### What is this repository for? ###

* Quick summary of the project
* How to get the system started in dev mode
* Issues with the project

### This project shows examples of a Django project put together with the metronic Theme UI ###

### How do I get set up? ###

* Ensure that python 3.6 or higher is installed
* load the system from Git
* Create a python virtual environment  https://docs.python.org/3/tutorial/venv.html
* Activate the Python Virtualenv and install all the Python requirements ```pip install -r requirements.txt```
* move to the metronic directory and start the dev server ```python manage.py runserver```

A sqlite DB with data is already included. This can be deleted and a new db created. to do this 
you will need to do a ```python manage.py migrate``` to create the db


### other info ###

The site has been set-up as close as I could get to your instructions. One area that did not work was the dropzone.js.
When I investigated, the dropzone.js library linked to from metronic is no longer available and the newer version does
not appear to work the same. There is a django-dropzone library but that was 6 years old. As such I have put in a standard
form for uploading files. This can be updated if you want to overlay with a different JS library for drag and drop file
downloads


