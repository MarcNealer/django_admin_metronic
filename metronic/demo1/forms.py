from django import forms
from .models import Order


class FileUploadForm(forms.Form):
    file = forms.FileField()


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['product', 'customer', 'number_of_items']