from django.urls import path

from .views import *

app_name = 'demo1'

urlpatterns = [
    path('', page1, name='dashboard'),
    path('loaddata/', load_data, name='loaddata'),
    path('exportdata/', export_data, name='exportdata'),
    path('datatable/', page2, name='datatable'),
    path('json_data/', json_data, name='json_data'),
    path('fileupload/', fileupload, name='file_upload'),
    path('thanks/', thanks, name='thanks'),
    path('neworder/', addorder, name='addorder'),
    path('deleteorder/<int:pk>/',deleteorder, name='deleteorder'),
    path('editorder/<int:pk>/', editorder, name='editorder')

]