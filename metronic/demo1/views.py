from django.shortcuts import render, redirect, HttpResponse, HttpResponseRedirect
from django.db import transaction
from django.http import JsonResponse, HttpResponseBadRequest
from .models import Order, Products, Customer, CustomerFile
from django.contrib.auth.models import User
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from .forms import FileUploadForm
import datetime

import json
# Create your views here.


def page1(request):
    return render(request, "index.html",{})


def page2(request):
    return render(request, "data_table.html", {})


def fileupload(request):
    if request.method == 'POST':
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            CustomerFile.objects.create(file=request.FILES['file'])
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FileUploadForm()

    return render(request, 'fileuploadnew.html', {'form': form})


def thanks(request):
    return render(request, 'thanks.html',{})


def addorder(request):
    if request.method == 'POST':
        errors = []
        if "product" not in request.POST:
            errors.append("Product is requried")
        if "customer" not in request.POST:
            errors.append("Customer is required")
        if "number_of_items" not in request.POST:
            errors.append("Number Of Items is required")
        if errors:
            return HttpResponseBadRequest(errors)
        vals = {'product': Products.objects.get(id=request.POST['product']),
                'customer': Customer.objects.get(id=request.POST['customer']),
                'number_of_items': request.POST['number_of_items'],
                'orderdate': datetime.datetime.now()}
        if 'status' in request.POST:
            vals['status'] = request.POST['status']
        if 'order_tags' in request.POST:
            tags = ",".join(request.POST.getlist('order_tags'))
            vals['order_tags'] = tags
        rec = Order.objects.create(**vals)
        return redirect('/datatable/')
    else:
        products = Products.objects.all().order_by("name")
        customers = Customer.objects.all().order_by("user__username")
        return render(request, 'neworder.html', {'products':products, 'customers': customers})


@csrf_exempt
def json_data(request):
    recs = Order.objects.all().values('id', 'product__name', 'number_of_items',
                                      'customer__user__username', 'status',
                                      'orderdate', 'order_tags').order_by("-id")
    fixed_data=[]
    for rec in recs:
        new_rec = rec
        new_rec['orderdate'] = rec['orderdate'].__str__()
        fixed_data.append(new_rec)

    return JsonResponse(fixed_data, safe=False)


def load_data(request):
    Order.objects.all().delete()
    Products.objects.all().delete()
    Customer.objects.all().delete()
    User.objects.all().delete()
    file = '{}/MetTestData.json'.format(settings.BASE_DIR)
    with open(file, 'r') as f:
        data = json.load(f)
    customers = {}
    products = set()
    for rec in data['orders']:
        customer = rec['customer']
        if customer['user']['username'] not in customers.keys():
            customers[customer['user']['username']] = customer
        products.add(rec['product']['name'])

    product_record = []
    for rec in products:
        product_record.append(Products(name=rec))
    Products.objects.bulk_create(product_record)

    user_records = []
    for user, customer in customers.items():
        user_records.append(User(username=user, email=customer['user']['email'],
                                 password=customer['user']['password']))

    User.objects.bulk_create(user_records)
    with transaction.atomic():
        for user, customer in customers.items():
            Customer.objects.create(user=User.objects.get(username=user), address=customer['address'])

    with transaction.atomic():
        for rec in data['orders']:
            Order.objects.create(product=Products.objects.get(name=rec['product']['name']),
                                 number_of_items=rec['number_of_items'],
                                 customer=Customer.objects.get(user__username=rec['customer']['user']['username']),
                                 status=rec['status'],
                                 orderdate=rec['orderdate'])
    return redirect('/')


def export_data(request):
    orders = Order.objects.all()
    data = {'orders':[]}
    for order in orders:
        rec = {'product':{'name':order.product.name},
               'number_of_items': order.number_of_items,
               'customer': {'user':{'username': order.customer.user.username,
                                    'email': order.customer.user.email,
                                    'password': 'testesta01'},
                            'address': order.customer.address},
               'status': order.status,
               'orderdate': order.orderdate.__str__()
               }
        data['orders'].append(rec)
    response = HttpResponse(content_type='text/json')
    response['Content-Disposition'] = 'attachment; filename="MetTestData.json"'
    json.dump(data, response, indent=4)
    return response


def deleteorder(request, pk):
    rec = get_object_or_404(Order, pk=pk)
    rec.delete()
    return redirect('/datatable/')


def editorder(request, pk):
    rec = get_object_or_404(Order, pk=pk)

    if request.method =='POST' or request.method == 'PUT':
        if 'product' in request.POST:
            prod_rec = get_object_or_404(Products, pk=request.POST['product'])
            rec.product = prod_rec
        if 'customer' in request.POST:
            cust_rec = get_object_or_404(Customer, pk=request.POST['customer'])
            rec.customer = cust_rec
        if 'status' in request.POST:
            rec.status = request.POST['status']
        if 'orderdate' in request.POST:
            rec.orderdate = request.POST['orderdate']
        if 'order_tags' in request.POST:
            tags = ",".join(request.POST.getlist('order_tags'))
            rec.order_tags = tags
        rec.save()
        return redirect('/datatable/')
    else:
        products = Products.objects.all().order_by("name")
        customers = Customer.objects.all().order_by("user__username")
        return render(request, 'editorder.html',{'record':rec,'products':products, 'customers': customers})
