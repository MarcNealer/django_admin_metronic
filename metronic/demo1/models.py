from django.db import models
from django.contrib.auth.models import User

# Create your models here.


STATUS = (
    ('NEW', 'NEW'),
    ('COMPLETED', 'COMPLETED')
)



class Products(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.TextField()

    def __str__(self):
        return self.user.username


class Order(models.Model):

    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    number_of_items = models.IntegerField(verbose_name='Number Of Items')
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS)
    orderdate = models.DateTimeField()
    ordercompleted = models.DateTimeField(blank=True, null=True)
    order_type = models.CharField(max_length=20, blank=True, null=True)
    order_tags = models.CharField(max_length=300)




class CustomerFile(models.Model):
    file = models.FileField(upload_to='customer_files')

